/**
*
* My solution to the Kruskal (MST): Really Special Subtree challenge on hackerrank.com
*
* Copyright (C) 2018 by Keith Jordan <3keithjordan3@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
* 
**/

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

// Nodes for use in a disjointed set.  As sets are joined via union, the set adopts a tree structure
class DisjointSetNode {

	DisjointSetNode *parent;
	int rank;
	int size;

public:

	// initialize a node as the only and representative member of the set
	DisjointSetNode() {
		parent = this;
		rank = 0;
		size = 1;
	}

	DisjointSetNode* getRoot();
	void Union(DisjointSetNode*);
	bool isDisjointed(DisjointSetNode*);
	DisjointSetNode* getParent() { return parent; }
	int getRank() { return rank; }
	int getSize() { return size; }
	void setParent(DisjointSetNode *newParent) { parent = newParent; }
	void rankUp() { ++rank; }
};

DisjointSetNode* DisjointSetNode::getRoot() {
	DisjointSetNode* n = this;
	while(n->getParent() != n) {
		n = n->getParent();
	}
	return n;
}

// Perform a union between the two sets.  Do union by rank, making the root of the tree with the
// larger rank the parent of the other root.  If the rank is equal, otherRoot becomes the parent
// of thisRoot, and increment the rank of otherRoot by one
void DisjointSetNode::Union(DisjointSetNode *otherNode) {
	DisjointSetNode *thisRoot = getRoot();
	DisjointSetNode *otherRoot = otherNode->getRoot();
	int newSize = thisRoot->getSize() + otherRoot->getSize();
	int newRank;

	if (thisRoot->getRank() > otherRoot->getRank()) {
		otherRoot->setParent(thisRoot);
	} else {
		thisRoot->setParent(otherRoot);
		if (thisRoot->getRank() == otherRoot->getRank())
			otherRoot->rankUp();
	}	
}

// test to see if the two nodes have the same root.  If not, they are in disjointed sets
bool DisjointSetNode::isDisjointed(DisjointSetNode *otherNode) {
	return (this->getRoot() != otherNode->getRoot());
}

class Edge {

	int vertex1;
	int vertex2;
	int weight;

public:

	Edge(int v1, int v2, int wt) {
		vertex1 = v1;
		vertex2 = v2;
		weight = wt;
	}

	int getWeight() const { return weight; }
	int getVertex1() const { return vertex1; } 
	int getVertex2() const { return vertex2; } 

};

// we compare edges by weight, but if weight is equal, we compare on v1 + v2 + weight
bool operator>(const Edge &a, const Edge &b) {
	if (a.getWeight() == b.getWeight()) {
		return (a.getWeight() + a.getVertex1() + a.getVertex2() >
				b.getWeight() + b.getVertex1() + b.getVertex2());
	}
	return (a.getWeight() > b.getWeight());
}

// the priority queue keeps a min heap of edges
typedef priority_queue<Edge, vector<Edge>, greater<Edge>> EdgePQueue;

void printEdges(EdgePQueue edgesPQueue) {
	while(!edgesPQueue.empty()) {
		Edge e = edgesPQueue.top();
		cout << e.getVertex1() + 1 << " -> " << e.getVertex2() + 1 << " weight: " << e.getWeight() << "\n";
		edgesPQueue.pop();
	}
}

// An implementation of Kruskals algorithm which returns the weight of the resultant graph
int getWeightKruskal(vector<DisjointSetNode*> &nodesVector, EdgePQueue &edgesPQueue) {

	int totalWeight = 0;
	int edgesAdded = 0;

	// add edges until there are numNodes - 1 edges
	while (edgesAdded < nodesVector.size() - 1) {
		// get next edge stats, pop edge from priority queue
		int vertex1 = edgesPQueue.top().getVertex1();
		int vertex2 = edgesPQueue.top().getVertex2();
		int edgeWeight = edgesPQueue.top().getWeight();
		edgesPQueue.pop();
		// test for a cycle
		if (nodesVector[vertex1]->isDisjointed(nodesVector[vertex2])) {
			// if none, add edge, add to weight and edgesAdded
			nodesVector[vertex1]->Union(nodesVector[vertex2]);
			totalWeight += edgeWeight;
			++edgesAdded;
		}
	}

	return totalWeight;

}

int main() {

	int numNodes, numEdges;
	cin >> numNodes >> numEdges;

	// Initialize our disjointed set nodes.  After this, each node is the sole element in a set.
	vector<DisjointSetNode*> nodesVector;
	for (int i = 0; i < numNodes; ++i) {
		nodesVector.push_back(new DisjointSetNode());
	}

	// a priority queue of edges.  A min heap which compares edges as specified in the above overloaded
	// comparison operator
	EdgePQueue edgesPQueue;

	for (int i = 0; i < numEdges; ++i) {
		int vertex1, vertex2, weight;
		cin >> vertex1 >> vertex2 >> weight;
		// Add a new edge.  Subtract one from the vertex id's, for nodeVector zero indexing
		edgesPQueue.push(Edge(vertex1 - 1, vertex2 - 1, weight));
	}

	//printEdges(edgesPQueue);

	int RSSTweight = getWeightKruskal(nodesVector, edgesPQueue);

	cout << RSSTweight << "\n";

	return 0;
}











