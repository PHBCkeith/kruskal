# Kruskal (MST): Really Special Subtree

This is my solution to the Kruskal (MST): Really Special Subtree problem on hackerrank.com

## Authors

* **Keith Jordan** - [contact](mailto:3keithjordan3@gmail.com)

## License

This project is licensed under the GNU General Public License - see the [COPYING](COPYING) file for details


